<?php $page_title = 'जनकल्याण नमुना माध्यमिक विद्यालय'; ?>
<!DOCTYPE html> 
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="description" content="">
	<title><?php echo $page_title; ?></title>
    <script src="<?php echo url_for('/vendors/js/nepdate.js')?>"></script>
	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo url_for('/img/icons/favicon.ico'); ?>" type="image/x-icon">
	<link rel="icon" href="<?php echo url_for('/img/icons/favicon.ico'); ?>" type="image/x-icon" type="image/x-icon">

	<!-- stylesheets -->
	<link rel="stylesheet" href="<?php echo url_for('/vendors/css/bootstrap.css'); ?>">
  <link rel="stylesheet" href="<?php echo url_for('/vendors/css/ionicons.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo url_for('/vendors/css/slick.css'); ?>">
  <link rel="stylesheet" href="<?php echo url_for('/vendors/css/slick-theme.css'); ?>">
	<link rel="stylesheet" href="<?php echo url_for('/vendors/css/ekko-lightbox.css'); ?>">
  <link href="https://fonts.googleapis.com/css?family=Bree+Serif|Muli" rel="stylesheet">

	<!-- custom stylesheet	 -->
	<link rel="stylesheet" href="<?php echo url_for('/resources/css/style.css'); ?>">
	<link rel="stylesheet" href="<?php echo url_for('/resources/css/media.css'); ?>">
	
</head>
<body>
<div id="bodyCover">
<div class="top_nav fixed-top">
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
      <div class="col-sm-8">
          <ul class="navbar-nav social">
              <li class="nav-item">
                  <a class="nav-link" href="https://www.facebook.com/jkmsts/" target="_blank"><i class="ion-social-facebook"></i></a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#"><i class="ion-social-twitter"></i></a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="#"><i class="ion-social-googleplus"></i></a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="https://www.youtube.com"><i class="ion-social-youtube"></i></a>
              </li>
             
          </ul>
      </div>
      <!--col-sm-4-->
      <div class="" style="display:flex; flex-direction: row; flex-wrap: wrap; justify-content: space-around;" >
          <form name="searchform" class="form-inline my-2 my-lg-0 form-margin" action="<?=url_for('/pages/?url=search')?>" method="POST">
              <input class="form-control" type="text"<?php if(isset($_POST['search'])){ ?>value="<?php echo $_POST['search']; ?>"<?php } ?>name="search" placeholder="Search" aria-label="Search" style="margin:0;">
            
              <button class="btn btn-info" type="submit"><i class="ion-search"></i></button>
          </form>
          
      </div>
      <a class="btn -btn-sm btn-light" style="color: #004b8e; margin-left: 10px;" href="<?php echo url_for('/admin/login.php'); ?>">Login</a>
      <ul class="navbar-nav">
          <li class="nav-item">
              <a href="https://webmail.jkmsts.edu.np/" target="_blank" class="nav-link" title="Check Email"><i class="ion-email" style="font-size:17px;"></i></a>
          </li>
      </ul>
      
    </div>
 </nav>
</div>
</div>


<!-- top logo banner -->
<section class="top-logo py-3">
	<div class="container">
    <div class="row">

      <div class="col-md-9">
      	<a class="head-link " href="<?php echo url_for('/'); ?>">
      		<img height="100px" class="img-responsive" style="float: left;" src="<?php echo WWW_ROOT.'/img/icons/logo.png' ?>" alt="School Logo">
        <?php //FetchName($con); ?>
       <!--<div class="address" style="font-size: 130%;">बागचौर नगरपालिका–२, थारमारे, सल्यान</div> 	-->
      	</a>
      </div>
      
      <div id="header-style">
          <!--hello-->
      </div>

      <!--<div class="col-md-3">-->
      <!--  <div class="social-icon">-->
      <!--    <?php FetchSocialHome($con); ?>-->
      <!--    <a class="btn -btn-sm btn-dark" href="<?php echo url_for('/admin/login.php'); ?>">Login</a>-->
      <!--  </div>-->
      <!--  <div class="clear-fix"></div>-->

      <!--  <div class="contact">-->
      <!--    <?php FetchContactHome($con); ?>-->
      <!--  </div>-->
      <!--</div>-->
    </div>		
	</div>
</section>
<!-- end top banner -->


<!-- navigation bar -->
<section class="navigation">
  <nav class="navbar navbar-expand-lg navbar-light bg-light mb-2">
  <a class="navbar-brand head-home hover-item"  href="<?php echo url_for('/'); ?>"><i class="ion-ios-home"></i> Home</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
            
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle hover-item" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          About Us
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="<?php echo url_for('/pages?url=about us&column=details'); ?>">Details</a>
          <a class="dropdown-item" href="<?php echo url_for('/pages?url=about us&column=history'); ?>">History</a>
          <a class="dropdown-item" href="<?php echo url_for('/pages?url=about us&column=profile'); ?>">School Profile</a>
          <a class="dropdown-item" href="<?php echo url_for('/pages?url=about us&column=management'); ?>">School Management</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle hover-item" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Academic Programs
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php selectProgram($con); ?>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link hover-item " href="<?php echo url_for('/pages?url=facilities') ?>">Facilities</a>
      </li>

      <li class="nav-item">
        <a class="nav-link hover-item" href="<?php echo url_for('/pages?url=jfss') ?>">JFSS</a>
      </li>

      <li class="nav-item">
        <a class="nav-link hover-item" href="<?php echo url_for('/pages?url=download') ?>">Download</a>
      </li>
      

      <li class="nav-item">
        <a class="nav-link hover-item" href="<?php echo url_for('/pages?url=gallery'); ?>">Gallery</a>
      </li>

      <li class="nav-item">
        <a class="nav-link hover-item" href="<?php echo url_for('/pages?url=career'); ?>">Career</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link hover-item" href="<?php echo url_for('/pages?url=result'); ?>">Result</a>
      </li>
     
    </ul>
    <!-- <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-sm btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form> -->
  </div>
  </nav>
</section>
<!-- navigation end -->