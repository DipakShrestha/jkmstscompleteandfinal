<?php 

function viewLatestNews($con){
	$query = mysqli_query($con, "SELECT * FROM news ORDER BY id DESC LIMIT 3");
	while ($row = mysqli_fetch_assoc($query)) {
		$id = $row['id'];
		$title = $row['title'];
		$image = $row['file_path'];
		$image = str_replace('../', '', $image);
		$content = $row['content'];
		$content = substr($content,0, 80);

		$res ="<div class='row news-list py-2'>
		<div class='col-lg-4'>
		<div class='news-img'>
		<img src='$image' class='img-fluid' alt=''>
		</div> 
		</div>
		<div class='col-lg-8'>
		<b>".ucwords($title)."</b>
		<p> $content ...</p>
		<a href='".url_for('/pages?url=news&id='.$id.'')."' class='btn btn-sm btn-outline-primary'>Read More</a>
		</div>
		</div>";

		echo $res;
	}
}

function LatestNewsPages($con){
	$query = mysqli_query($con, "SELECT * FROM news ORDER BY id DESC LIMIT 3");
	while ($row = mysqli_fetch_assoc($query)) {
		$id = $row['id'];
		$title = $row['title'];
		$image = $row['file_path'];
		$content = $row['content'];
		$content = substr($content,0, 80);

		$res ="<div class='row news-list py-2'>
		<div class='col-lg-4'>
		<div class='news-img'>
		<img src='$image' class='img-fluid' alt=''>
		</div> 
		</div>
		<div class='col-lg-8'>
		<b>".ucwords($title)."</b>
		<p> $content ...</p>
		<a href='".url_for('/pages?url=news&id='.$id.'')."' class='btn btn-sm btn-outline-primary'>Read More</a>
		</div>
		</div>";

		echo $res;
	}
}

function marqueeNews($con){
    // $query = mysqli_query($con,"SELECT MAX(id) FROM news");
    // $id=mysqli_fetch_assoc($query);
    // $id=$id['MAX(id)'];
    // $query = mysqli_query($con,"SELECT id,title FROM news WHERE id=$id");
    $query = mysqli_query($con,"SELECT * from news ORDER BY id DESC");
    // $row = mysqli_fetch_assoc($query);
    // console.log($row);
    while($row = mysqli_fetch_assoc($query)){
        $id = $row['id'];
        $res = "<span>
                    <a href='".url_for('/pages?url=news&id='.$id.'')."' class='latest_marquee_news' style='font-family: Impact; font-size: 12pt; margin-right:50vw;'>
                        ".ucwords($row['title'])."
                    </a>
                </span>";
        echo $res;
    }
    
}
function bannerNews($con){
	$query = mysqli_query($con, "SELECT * FROM news ORDER BY id DESC LIMIT 3");
	while ($row = mysqli_fetch_assoc($query)) {
		$id = $row['id'];
		$title = $row['title'];
		$content = $row['content'];
		$content = substr($content,0, 80);

		$res ="<div class='row news-list py-2'>
		<div class='col-lg-12'>
		<b>".ucwords($title)."</b>
		$content
		<a href='".url_for('/pages?url=news&id='.$id.'')."' class='btn btn-sm btn-outline-primary'>Read More</a>
		</div>
		</div>";

		echo $res;
	}
}


function FetchName($con){
	$query = mysqli_query($con, "SELECT school_name FROM homepage");
	while ($row = mysqli_fetch_assoc($query)) {
		$name = hsc($row['school_name']);
		
		$res ="
		<h2 class='pt-3'>$name</h2>";
		echo $res;
	}
}

function FetchSocialHome($con){
	$query = mysqli_query($con, "SELECT facebook, twitter, youtube FROM homepage");
	while ($row = mysqli_fetch_assoc($query)) {
		$fb = hsc($row['facebook']);
		$tw = hsc($row['twitter']);
		$yt = hsc($row['youtube']);

		$res ="<li><a href='$fb' target='_blank'><i class='ion-social-facebook'></i></a></li>
		<li><a href='$tw' target='_blank'><i class='ion-social-twitter'></i></a></li>
		<li><a href='$yt' target='_blank'><i class='ion-social-youtube'></i></a></li>";

		echo $res;
	}
}


function FetchContactHome($con){
	$query = mysqli_query($con, "SELECT phone,  email FROM homepage");
	while ($row = mysqli_fetch_assoc($query)) {
		$phone = hsc($row['phone']);
		$email = hsc($row['email']);

		$res ="
		<li><i class='ion-android-call'></i>&nbsp;&nbsp;$phone </li>
		<li><i class='ion-email'></i>&nbsp;&nbsp;$email</li>";

		echo $res;
	}
}

function ContactFooter($con){
	$query = mysqli_query($con, "SELECT school_name, address, ward, phone,  email FROM homepage");
	while ($row = mysqli_fetch_assoc($query)) {
		$name = hsc($row['school_name']);
		$address = hsc($row['address']);
		$ward = hsc($row['ward']);
		$phone = hsc($row['phone']);
		$email = hsc($row['email']);

		$res ="
		<li>
		<i class='ion-ios-location'></i>&nbsp;&nbsp;<span>$name</span>
		<div>$address</div>
		<div>$ward</div>
		</li>
		<li>
		<i class='ion-android-call'></i>&nbsp;&nbsp;<span>Phone no :</span>
		<div>$phone</div>
		</li>
		<li>
		<i class='ion-email'></i>&nbsp;&nbsp;
		<span>$email</span>
		</li>";

		echo $res;
	}
}



function selectProgram($con){
	$query = mysqli_query($con, "SELECT id,title FROM programs");
	while ($row = mysqli_fetch_assoc($query)) {
		$name = hsc($row['title']);
		$prog_id = hsc($row['id']);

		echo "<a class='dropdown-item' href='".url_for('/pages?url=academic program&id='.$prog_id.'')."'>$name</a>";
	}
}


function viewProgram($con, $prog_id){
	$query = mysqli_query($con, "SELECT * FROM programs WHERE id ='$prog_id' LIMIT 1");
	while ($row = mysqli_fetch_assoc($query)) {
		$title = $row['title'];


		$image = $row['file_path'] ? $row['file_path'] : '../img/School.jpg';


		$content = $row['content'];

		$res ="<div id='about-content'>
		<span>$title</span>
		<div class='program-image'>
		<img style='max-height:500px;' src='$image' alt='image' class='img-fluid'>
		</div>
		<div>$content</div>
		</div>";

		echo $res;


	} 
}


function messagePrincipal($con){
	$query = mysqli_query($con, "SELECT * FROM message WHERE id=1");
	while ($row = mysqli_fetch_assoc($query)) {
		$name = hsc($row['name']);
		$id = $row['id'];
		
		$message = $row['message'];
		$photo = $row['file_path'];
		$photo = str_replace('../', '', $photo);
        $message = substr($message,0, 200);
		$res = "<div style='align-item:center;'><div class='message-img' style='align-text:center;'>
            		<img src='$photo' alt=''>
            	</div>
            	<p class='message-man text-center'>Mr. ".ucwords($name)."</p>
            
            	<p class='message-body'>
            		$message...
            	</p>
            	<button type='button' class='btn btn-primary' style='background: #004b8e;margin-left:15vw;' data-toggle='modal' data-target='#myModal1'>
                    Readmore
                </button></div>";
		echo $res;
	}
}


function messageChairman($con){
	$query = mysqli_query($con, "SELECT * FROM message WHERE id=2");
	while ($row = mysqli_fetch_assoc($query)) {
		$name = hsc($row['name']);
		$message = $row['message'];
		$photo = $row['file_path'];
		$photo = str_replace('../', '', $photo);
        $message = substr($message,0, 200);
		$res = "<div class='message-img'>
		<img src='$photo' alt=''>
		</div>
		<p class='message-man text-center'>Mr. ".ucwords($name)."</p>

		<p class='message-body'>
		$message...
		</p>
		<button type='button' class='btn btn-primary' style='background: #004b8e;margin-left:15vw;' data-toggle='modal' data-target='#myModal2'>
                Readmore
            </button>";
		echo $res;
	}
}



function fetchTestimonial($con){
	$query = mysqli_query($con, "SELECT * FROM testimonial");
	while ($row = mysqli_fetch_assoc($query)) {
		$name = $row['name'];
		$id = $row['id'];
		$GLOBALS['extra_name'] = 'dipak me hello';
		$image = $row['file_path'];
		$image = str_replace('../', '', $image);
		$content = $row['content'];
        $content = substr($content,0, 500);
    
            $ret ="<div class='col-md-4 text-center mt-4'>
		            <div class='testimonial-img'>
		                <img src='$image' alt=''>
		            </div>
		            <p>$content...<span><button type='button' class='btn btn-primary' style='background: #004b8e;' data-toggle='modal' data-target='#myModal$id'>
                      Readmore
                    </button>
                    </span></p>
		            <b>$name</b>
		        </div>";
		$res ="<div class='mt-4' style='display:block; text-align: center; flex-direction:column;'>
		            <div class='testimonial-img'>
		                <img src='$image' alt=''>
		            </div>
		            <div>$content...
    		            <span><button type='button' class='btn btn-primary' style='background: #004b8e;' data-toggle='modal' data-target='#myModal$id'>
                                Readmore
                            </button>
                        </span>
                    </div>
		            <b>$name</b>
		        </div>";
		echo $ret;
	}
}

function fetchTestimonialContent($con){
	$query = mysqli_query($con, "SELECT * FROM testimonial");
	while ($row = mysqli_fetch_assoc($query)) {
		$name = $row['name'];
		$id = $row['id'];
		$image = $row['file_path'];
		$image = str_replace('../', '', $image);
		$content = $row['content'];
		$res ='
        <!-- The Modal -->
        <div class="modal" id="myModal'.$id.'">
          <div class="modal-dialog">
            <div class="modal-content" style="color:#000">
        
              <!-- Modal Header -->
              <div class="modal-header">
                <h4 class="modal-title" style="color: #004b8e;">'.$name.'</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
        
              <!-- Modal body -->
              <div class="modal-body" style="box-shadow: 0 0 0 5px rgba(200,200,200,0.1) inset, 0px 0 3px 0 rgba(0,0,0,0.35);border-radius:5px;margin:9px;">
                <div class="testimonial-img">
        		   <img src="'.$image.'" alt="">
        		</div>
        		<p>'.$content.'</p>
              </div>
        
              <!-- Modal footer -->
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" style="color:#fff; background: #004b8e;" data-dismiss="modal">Close</button>
              </div>
        
            </div>
          </div>
        </div>';
	echo $res;
	}
}

?>