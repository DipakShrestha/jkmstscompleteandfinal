<?php 
ob_start();
session_start();

// events
 function createEvents($con, $error_array){
 	if (isset($_POST['add_event'])) {
 		$date = db_escape($con, $_POST['date']);
 		$title = db_escape($con, $_POST['event_title']);
 		$body = db_escape($con, $_POST['event_body']);
 		

 		$query=mysqli_query($con, "INSERT INTO events(date, title, body) VALUES('$date', '$title', '$body')");
 		if ($query) {
 			$l = url_for('admin/index.php?url=event');
 			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=event';window.location.href=e;</script>";
 			header('location:'.$l.'');
 		}
 	} 
 }

 function viewEvents($con){
 	$query = mysqli_query($con, "SELECT * FROM events ORDER BY date");
 	$counter =1;
 	while ($row = mysqli_fetch_assoc($query)) {
 		$id = url($row['id']);
 		$event_title = $row['title'];
 		$event_body = $row['body'];
 		$date = $row['date'];
 		$yrdata = strtotime($date);

 		$res ="<tr>
 		<td>$counter</td>
 		<td><a style='color:black;' href='".url_for('/admin/index.php?url=delete&id='.$id.'')."'>".hsc(ucfirst($event_title))."</a></td>
 		<td>".hsc(ucfirst($event_body))."</td>
 		<td class='ddate".$counter."'>".$date."</td>
 		<td><a class='btn btn-sm btn-danger' href='".url_for('/admin/index.php?url=delete&eventId='.$id.'')."'>Delete</a></td>
 		</tr><script type='text/javascript'>
                var date='".$date."';date=ad2bs(date.split('-').join('/'));
                date=[date.ne.year,date.ne.month,date.ne.day];
                date=date.join('/');
                document.getElementsByClassName('ddate".$counter."')[0].innerHTML=date;
              </script>";

 		echo $res;			
 		++$counter;
 	}
 }

 function deleteEvent($con, $event_id){
 	if (isset($_POST["del_yes"])) {
 		$query = mysqli_query($con, "DELETE FROM events WHERE id='".db_escape($con, $event_id)."' LIMIT 1" );
 		if ($query) {
 			$l = url_for('admin/index.php?url=event');
 			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=event';window.location.href=e;</script>";
 			header("Location: $l ");
 			
 		}
 	}else{
 		if (isset($_POST["del_no"])) {
 			$l = url_for('admin/index.php?url=event');
 			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=event';window.location.href=e;</script>";
 			header("Location: $l ");
 		}
 	}
 }


// categories
 function createCategories($con, $error_array){
 	if (isset($_POST['add_cat'])) {
 		$cat = db_escape($con, $_POST['category']);

 		$query=mysqli_query($con, "INSERT INTO categories(name) VALUES('$cat')");
 		if ($query) {
 			$l = url_for('admin/index.php?url=category');
 			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=category';window.location.href=e;</script>";
 			header("Location: $l ");
 		}
 	} 
 }

 function viewCategories($con){
 	$query = mysqli_query($con, "SELECT * FROM categories");
 	$counter =1;
 	while ($row = mysqli_fetch_assoc($query)) {
 		$cat_id = url($row['id']);
 		$cat_name = $row['name'];

 		$res ="<tr>
 		<td>$counter</td>
 		<td>".hsc(ucfirst($cat_name))."</td>
 		<td><a style='color:black;' href='".url_for('/admin/index.php?url=delete&catId='.$cat_id.'')."'>Delete &nbsp;<i class='ion-ios-trash-outline'></i></a></td>
 		</tr>";

 		echo $res;			
 		++$counter;
 	}
 }

 function selectCategory($con){
 	$query = mysqli_query($con, "SELECT * FROM categories");
 	while ($row = mysqli_fetch_assoc($query)) {
 		$cat_id = hsc($row['id']);
 		$cat_name = hsc($row['name']);

 		echo "<option value='".$cat_id."'>".$cat_name."</option>";
 	}
 }

 function deleteCategory($con, $cat_id){
 	if (isset($_POST["del_yes"])) {
 		$query = mysqli_query($con, "DELETE FROM categories WHERE id='".db_escape($con,$cat_id)."' LIMIT 1" );
 		if ($query) {
 			$l = url_for('admin/index.php?url=category');
 			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=category';window.location.href=e;</script>";
 			header("Location: $l ");
 		}
 	}else{
 		if (isset($_POST["del_no"])) {
 			$l = url_for('admin/index.php?url=category');
 			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=category';window.location.href=e;</script>";
 			header("Location: $l ");
 		}
 	}
 }





// *******************
// *********POSTS*****
// *******************
 function submitPost($con){
 	if (isset($_POST['submit_post'])) {
 		$title = $_POST['heading'];
 		$cat = $_POST['category'];
 		$content = $_POST['post-content'];
 		$date = $_POST['date'];

 		$post_image = $_FILES['fileToUpload']['name'];
 		if ($post_image != "") {
 			$target_dir = "../img/posts/";
 			$target_file = $target_dir .uniqid(). basename($post_image);
 			$uploadOk = 1;
 			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

			// Check if image file is a actual image or fake image
 			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
 			if($check !== false) {
 				echo "File is an image - " . $check["mime"] . ".";
 				$uploadOk = 1;
 			} else {
 				echo "File is not an image.";
 				$uploadOk = 0;
 			}

			// Check if file already exists
 			if (file_exists($target_file)) {
 				echo "Sorry, file already exists.";
 				$uploadOk = 0;
 			}
			// Check file size
 			if ($_FILES["fileToUpload"]["size"] > 5000000) {
 				echo "Sorry, your file is too large.";
 				$uploadOk = 0;
 			}
			// Allow certain file formats
 			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
 				&& $imageFileType != "gif" ) {
 				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
 			$uploadOk = 0;
 		}
			// Check if $uploadOk is set to 0 by an error
 		if ($uploadOk == 0) {
 			echo "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
 		} 
 		else {
 			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
 				echo "The file ". basename($post_image). " has been uploaded.";
 				$query = mysqli_query($con, "INSERT INTO news(title, cat_id, file_path, content, date) VALUES( '$title', '$cat', '$target_file','$content', '$date')");

 			} else {
 				echo "Sorry, there was an error uploading your file.";
 			}
 		}	
 	}
 	else{
 		$query = mysqli_query($con, "INSERT INTO news(title, cat_id, file_path, content, date) VALUES('$title', '$cat', ' ','$content', '$date')");
 		if (!$query) {
 			echo mysqli_error($con);
 		}
 	}
 }
}


function editPost($con, $post_id){
	if (isset($_POST['edit_post'])) {
		$title = $_POST['heading'];
		$cat = $_POST['category'];
		$content = $_POST['post-content'];
		$date = $_POST['date'];

		$post_image = $_FILES['fileToUpload']['name'];
		if ($post_image != "") {
			$target_dir = "../img/posts/";
			$target_file = $target_dir .uniqid(). basename($post_image);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

			// Check if image file is a actual image or fake image
			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			if($check !== false) {
				echo "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} else {
				echo "File is not an image.";
				$uploadOk = 0;
			}

			// Check if file already exists
			if (file_exists($target_file)) {
				echo "Sorry, file already exists.";
				$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 5000000) {
				echo "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
			// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			echo "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
		} 
		else {
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
				echo "The file ". basename($post_image). " has been uploaded.";
				$query = mysqli_query($con, "UPDATE news SET title = '$title', cat_id = '$cat', file_path = '$target_file', content ='$content', date='$date' WHERE id = '$post_id' ");
				if ($query) {
					$l = url_for('admin/index.php?url=editNews&post_id='.$post_id);
					header("Location: $l ");
				}
				if (!$query) {
					echo mysqli_error($con);
				}
			} else {
				echo "Sorry, there was an error uploading your file.";
			}
		}	
	}else{
		$query = mysqli_query($con, "UPDATE news SET title = '$title', cat_id = '$cat', content ='$content', date='$date' WHERE id = '$post_id' ");
		if ($query) {
			$l = url_for('admin/index.php?url=editNews&post_id='.$post_id);
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=editNews&post_id=".$post_id."';window.location.href=e;</script>";
			header("Location: $l ");
		}
		if (!$query) {
			echo mysqli_error($con);
		}
	}
}
}




function deletePost($con, $post_id){
	if (isset($_POST["del_yes"])) {
		$query = mysqli_query($con, "DELETE FROM news WHERE id='".db_escape($con, $post_id)."' LIMIT 1" );
		if ($query) {
			$l = url_for('admin/index.php?url=view news');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=view news';window.location.href=e;</script>";
			header("Location: $l ");
		}
	}else{
		if (isset($_POST["del_no"])) {
			$l = url_for('admin/index.php?url=view news');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=view news';window.location.href=e;</script>";
			header("Location: $l ");
		}
	}
}


// ****************
// HOMEPAGE
// ****************
function selectAboutUslim($con){
	$query = mysqli_query($con, "SELECT about_us FROM homepage");
	while ($row = mysqli_fetch_assoc($query)) {
		$content = $row['about_us'];
		$content = substr($content,0, 2882);

		echo "<div class='text-justify'>".$content."</div>";
	}
} 


function selectAboutUs($con, $column){
	$query = mysqli_query($con, "SELECT $column FROM about_us");
	while ($row = mysqli_fetch_assoc($query)) {
		$content = $row[$column];

		echo "<div class='my-4'>$content</div>";
	}
} 

function submitAboutUsColumn($con, $column){
	if (isset($_POST['submit'])) {
		$content = db_escape($con, $_POST['post-content']);

		$query=mysqli_query($con, "UPDATE about_us SET $column ='$content' WHERE id='1' ");
		if ($query) {
			$l = url_for('admin/index.php?url=about us&column='.$column);
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=about us&column=".$column."';window.location.href=e;</script>";
			header("Location: $l ");
		}else{
			echo mysqli_error($con);
		}
	} 
}


function updateSocial($con){
	if (isset($_POST['submit'])) {
		$fb = db_escape($con, $_POST['fb_link']);
		$tw = db_escape($con, $_POST['tw_link']);
		$yt= db_escape($con, $_POST['yt_link']);


		$query=mysqli_query($con, "UPDATE homepage SET facebook ='$fb', twitter ='$tw', youtube ='$yt' WHERE id='1' ");
		if ($query) {
			$l = url_for('admin/index.php?url=social');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=social';window.location.href=e;</script>";
			header("Location: $l ");
		}else{
			echo mysqli_error($con);
		}
	} 
}

function fetchSocial($con){
	$query = mysqli_query($con, "SELECT facebook, twitter, youtube FROM homepage");
	while ($row = mysqli_fetch_assoc($query)) {
		$fb = hsc($row['facebook']);
		$tw = hsc($row['twitter']);
		$yt = hsc($row['youtube']);


		$res ="<div class='row pb-2'>
		<div class='col-md-2  text-center'>Facebook link :</div>
		<div class='col-md-9'>
		<input type='text' class='form-control' name='fb_link' value='$fb'>
		</div>
		</div>

		<div class='row pb-2'>
		<div class='col-md-2  text-center'>Twitter link :</div>
		<div class='col-md-9'>
		<input type='text'  class='form-control' name='tw_link' value='$tw'>
		</div>
		</div>

		<div class='row pb-2'>
		<div class='col-md-2  text-center'>Youtube link :</div>
		<div class='col-md-9'>
		<input type='text'  class='form-control' name='yt_link' value='$yt'>
		</div>
		</div>";

		echo $res;
	}
}



function fetchContact($con){
	$query = mysqli_query($con, "SELECT school_name, address, ward, phone,  email FROM homepage");
	while ($row = mysqli_fetch_assoc($query)) {
		$name = hsc($row['school_name']);
		$address = hsc($row['address']);
		$ward = hsc($row['ward']);
		$phone = hsc($row['phone']);
		$email = hsc($row['email']);


		$res ="<div class='row pb-2'>
		<div class='col-md-2  text-center'>School Name :</div>
		<div class='col-md-9'>
		<input type='text' class='form-control' name='school' value='$name'>
		</div>
		</div>

		<div class='row pb-2'>
		<div class='col-md-2  text-center'>Address 1:</div>
		<div class='col-md-9'>
		<input type='text' class='form-control' name='address' value='$address'>
		</div>
		</div>

		<div class='row pb-2'>
		<div class='col-md-2  text-center'>Address 2:</div>
		<div class='col-md-9'>
		<input type='text'  class='form-control' name='ward' value='$ward'>
		</div>
		</div>

		<div class='row pb-2'>
		<div class='col-md-2  text-center'>Phone no. :</div>
		<div class='col-md-9'>
		<input type='text'  class='form-control' name='phone' value='$phone'>
		</div>
		</div>

		<div class='row pb-2'>
		<div class='col-md-2  text-center'>Email :</div>
		<div class='col-md-9'>
		<input type='text'  class='form-control' name='email' value='$email'>
		</div>
		</div>";

		echo $res;
	}
}

function updateContact($con){
	if (isset($_POST['submit'])) {
		$name = db_escape($con, $_POST['school']);
		$address = db_escape($con, $_POST['address']);
		$ward = db_escape($con, $_POST['ward']);
		$phone = db_escape($con, $_POST['phone']);
		$email = db_escape($con, $_POST['email']);


		$query=mysqli_query($con, "UPDATE homepage SET school_name = '$name', address = '$address', ward ='$ward', phone='$phone',  email='$email' WHERE id='1' ");
		if ($query) {
			$l = url_for('admin/index.php?url=contact');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=contact';window.location.href=e;</script>";
			header("Location: $l ");
		}else{
			echo mysqli_error($con);
		}
	} 
}



function submitMessage($con, $id){
	if (isset($_POST['submit'])) {
		$name = db_escape($con, $_POST['name']);
		$message = db_escape($con, $_POST['post-content']);

		$post_image = $_FILES['fileToUpload']['name'];
		if ($post_image != "") {
			$target_dir = "../img/people/";
			$target_file = $target_dir .uniqid(). basename($post_image);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

			// Check if image file is a actual image or fake image
			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			if($check !== false) {
				echo "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} else {
				echo "File is not an image.";
				$uploadOk = 0;
			}

			// Check if file already exists
			if (file_exists($target_file)) {
				echo "Sorry, file already exists.";
				$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 5000000) {
				echo "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
			// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			echo "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
		} 
		else {
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
				echo "The file ". basename($post_image). " has been uploaded.";
				$query = mysqli_query($con, "UPDATE message SET message='$message', file_path='$target_file', name ='$name' WHERE id = '$id' ");
				if ($query) {
					$l = url_for('admin/index.php?url=message&id='.$id.'');
					echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=message&id=".$id."';window.location.href=e;</script>";
					header("Location: $l ");
				}

			} else {
				echo "Sorry, there was an error uploading your file.";
			}
		}	
	}
	else{
		$query = mysqli_query($con, "UPDATE message SET message='$message', name ='$name' WHERE id = '$id' ");
		if ($query) {
			$l = url_for('admin/index.php?url=message&id='.$id.'');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=message';window.location.href=e;</script>";
			header("Location: $l ");
		}
	}
}
} 



function submitTestimonial($con){
 	if (isset($_POST['submit'])) {
 		$name = $_POST['name'];
 		$content = $_POST['post-content'];

 		$post_image = $_FILES['fileToUpload']['name'];
 		if ($post_image != "") {
 			$target_dir = "../img/people/";
 			$target_file = $target_dir .uniqid(). basename($post_image);
 			$uploadOk = 1;
 			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

			// Check if image file is a actual image or fake image
 			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
 			if($check !== false) {
 				echo "File is an image - " . $check["mime"] . ".";
 				$uploadOk = 1;
 			} else {
 				echo "File is not an image.";
 				$uploadOk = 0;
 			}

			// Check if file already exists
 			if (file_exists($target_file)) {
 				echo "Sorry, file already exists.";
 				$uploadOk = 0;
 			}
			// Check file size
 			if ($_FILES["fileToUpload"]["size"] > 5000000) {
 				echo "Sorry, your file is too large.";
 				$uploadOk = 0;
 			}
			// Allow certain file formats
 			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
 				&& $imageFileType != "gif" ) {
 				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
 			$uploadOk = 0;
 		}
			// Check if $uploadOk is set to 0 by an error
 		if ($uploadOk == 0) {
 			echo "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
 		} 
 		else {
 			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
 				echo "The file ". basename($post_image). " has been uploaded.";
 				$query = mysqli_query($con, "INSERT INTO testimonial(name, content, file_path) VALUES( '$name','$content', '$target_file')");

 			} else {
 				echo "Sorry, there was an error uploading your file.";
 			}
 		}	
 	}
 	else{
 		$query = mysqli_query($con, "INSERT INTO testimonial(name, content,file_path ) VALUES('$name','$content', '')");
 		if (!$query) {
 			echo mysqli_error($con);
 		}
 	}
 }
}


function deleteTestimonial($con, $t_id){
	if (isset($_POST["del_yes"])) {
		$query = mysqli_query($con, "DELETE FROM testimonial WHERE id='".db_escape($con, $t_id)."' LIMIT 1" );
		if ($query) {
			$l = url_for('admin/index.php?url=testimonial');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=testimonial';window.location.href=e;</script>";
			header("Location: $l ");
		}
	}else{
		if (isset($_POST["del_no"])) {
			$l = url_for('admin/index.php?url=testimonial');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=testimonial';window.location.href=e;</script>";
			header("Location: $l ");
		}
	}
}
/*******************
***Program*******
*******************/

function submitProgram($con){
	if (isset($_POST['submit_post'])) {
		$title = db_escape($con, $_POST['heading']);
		$content = db_escape($con, $_POST['post-content']);

		$post_image = $_FILES['fileToUpload']['name'];
		if ($post_image != "") {
			$target_dir = "../img/posts/";
			$target_file = $target_dir .uniqid(). basename($post_image);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

			// Check if image file is a actual image or fake image
			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			if($check !== false) {
				echo "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} else {
				echo "File is not an image.";
				$uploadOk = 0;
			}

			// Check if file already exists
			if (file_exists($target_file)) {
				echo "Sorry, file already exists.";
				$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 5000000) {
				echo "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
			// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			echo "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
		} 
		else {
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
				echo "The file ". basename($post_image). " has been uploaded.";
				$query = mysqli_query($con, "INSERT INTO programs(title, file_path, content) VALUES( '$title', '$target_file','$content')");

			} else {
				echo "Sorry, there was an error uploading your file.";
			}
		}	
	}
	else{
		$query = mysqli_query($con, "INSERT INTO programs(title, file_path, content) VALUES('$title', '','$content')");
		if (!$query) {
			echo mysqli_error($con);
		}
	}
}
}


function editProgram($con, $prog_id){
	if (isset($_POST['edit_post'])) {
		$title = $_POST['heading'];
		$content = $_POST['post-content'];

		$post_image = $_FILES['fileToUpload']['name'];
		if ($post_image != "") {
			$target_dir = "../img/posts/";
			$target_file = $target_dir .uniqid(). basename($post_image);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

			// Check if image file is a actual image or fake image
			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			if($check !== false) {
				echo "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} else {
				echo "File is not an image.";
				$uploadOk = 0;
			}

			// Check if file already exists
			if (file_exists($target_file)) {
				echo "Sorry, file already exists.";
				$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 5000000) {
				echo "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
			// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			echo "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
		} 
		else {
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
				echo "The file ". basename($post_image). " has been uploaded.";
				$query = mysqli_query($con, "UPDATE programs SET title = '$title', file_path = '$target_file', content ='$content' WHERE id = '$prog_id' ");
				if ($query) {
					$l = url_for('admin/index.php?url=edit program&prog_id='.$prog_id.'');
						echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=edit program&prog_id=".$prog_id."';window.location.href=e;</script>";
					header("Location: $l ");
				}
				if (!$query) {
					echo mysqli_error($con);
				}
			} else {
				echo "Sorry, there was an error uploading your file.";
			}
		}	
	}else{
		$query = mysqli_query($con, "UPDATE programs SET title = '$title', content ='$content' WHERE id = '$prog_id' ");
		if ($query) {
			$l = url_for('admin/index.php?url=edit program&prog_id='.$prog_id.'');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=edit program&prog_id=".$prog_id."';window.location.href=e;</script>";
			header("Location: $l ");
		}
		if (!$query) {
			echo mysqli_error($con);
		}
	}
}
}


function deleteProgram($con, $prog_id){
	if (isset($_POST["del_yes"])) {
		$query = mysqli_query($con, "DELETE FROM programs WHERE id='".db_escape($con, $prog_id)."' LIMIT 1" );
		if ($query) {
			$l = url_for('admin/index.php?url=view program');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=view program&prog_id=".$prog_id."';window.location.href=e;</script>";
			header("Location: $l ");
		}
	}else{
		if (isset($_POST["del_no"])) {
			$l = url_for('admin/index.php?url=view program');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=view program&prog_id=".$prog_id."';window.location.href=e;</script>";
			header("Location: $l ");
		}
	}
}

/*******************
***Facilities*******
*******************/
function submitFacility($con){
 	if (isset($_POST['submit'])) {
 		$name = $_POST['name'];
 		$content = $_POST['post-content'];

 		$post_image = $_FILES['fileToUpload']['name'];
 		if ($post_image != "") {
 			$target_dir = "../img/posts/";
 			$target_file = $target_dir .uniqid(). basename($post_image);
 			$uploadOk = 1;
 			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

			// Check if image file is a actual image or fake image
 			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
 			if($check !== false) {
 				echo "File is an image - " . $check["mime"] . ".";
 				$uploadOk = 1;
 			} else {
 				echo "File is not an image.";
 				$uploadOk = 0;
 			}

			// Check if file already exists
 			if (file_exists($target_file)) {
 				echo "Sorry, file already exists.";
 				$uploadOk = 0;
 			}
			// Check file size
 			if ($_FILES["fileToUpload"]["size"] > 5000000) {
 				echo "Sorry, your file is too large.";
 				$uploadOk = 0;
 			}
			// Allow certain file formats
 			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
 				&& $imageFileType != "gif" ) {
 				echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
 			$uploadOk = 0;
 		}
			// Check if $uploadOk is set to 0 by an error
 		if ($uploadOk == 0) {
 			echo "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
 		} 
 		else {
 			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
 				echo "The file ". basename($post_image). " has been uploaded.";
 				$query = mysqli_query($con, "INSERT INTO facilities(name, file_path, content) VALUES( '$name','$target_file', '$content')");
			 		if ($query) {
			 			$l = url_for('admin/index.php?url=facilities');
			 			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=facilities';window.location.href=e;</script>";
						header("Location: $l ");
			 		}
 			} else {
 				echo "Sorry, there was an error uploading your file.";
 			}
 		}	
 	}
 	else{
 		$query = mysqli_query($con, "INSERT INTO facilities(name, file_path, content) VALUES('$name','','$content')");

 		if ($query) {
 			$l = url_for('admin/index.php?url=facilities');
 			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=facilities';window.location.href=e;</script>";
			header("Location: $l ");
 		}
 	}
 }
}


function deleteFacility($con, $f_id){
	if (isset($_POST["del_yes"])) {
		$query = mysqli_query($con, "DELETE FROM facilities WHERE id='".db_escape($con, $f_id)."' LIMIT 1" );
		if ($query) {
			$l = url_for('admin/index.php?url=facilities');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=facilities';window.location.href=e;</script>";
			header("Location: $l ");
		}
	}else{
		if (isset($_POST["del_no"])) {
			$l = url_for('admin/index.php?url=facilities');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=facilities';window.location.href=e;</script>";
			header("Location: $l ");
		}
	}
}


function jsff($con,$id){
	if (isset($_POST['submit'])) {
		$body = db_escape($con, $_POST['post-content']);
        if($body==""){
           echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=jfss';window.location.href=e;</script>"; 
        }else{
            if(isset($id)){$query=mysqli_query($con, "UPDATE jfss SET name='$body' WHERE id=$id");
            }else{
            $query=mysqli_query($con, "INSERT into jfss(name) values('$body')");
            }
    		if ($query) {
    			$l = url_for('admin/index.php?url=jfss');
    			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=jfss';window.location.href=e;</script>";
    			header("Location: $l ");
    		}else{
    			echo mysqli_error($con);
    		}
        }
// 		
	} 
	
	$id = isset($_POST['id']);
	echo $id+1;
	if (isset($_POST['edit'])) {
	    $id_h = $id + 1;
		$body = db_escape($con, $_POST['edit-delete-content'.$id_h]);

		$query=mysqli_query($con, "UPDATE jfss SET name='$body' where id='$id'");
        // $query=mysqli_query($con, "INSERT into jfss(name) values('$body')");
		if ($query) {
			$l = url_for('admin/index.php?url=jfss');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=jfss';window.location.href=e;</script>";
			header("Location: $l ");
		}else{
			echo mysqli_error($con);
		}
	} 
}

function jsffEdit($con){
    $id = isset($_GET['id']);
	if (isset($_POST['edit'])) {
		$body = db_escape($con, $_POST['edit-delete-content'.$id]);

		$query=mysqli_query($con, "UPDATE jfss SET name='$body' WHERE id=$id");
        // $query=mysqli_query($con, "INSERT into jfss(name) values('$body')");
		if ($query) {
			$l = url_for('admin/index.php?url=jfss');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=jfss';window.location.href=e;</script>";
			header("Location: $l ");
		}else{
			echo mysqli_error($con);
		}
	} 
}


function submitFile($con){
 	if (isset($_POST['submit'])) {
 		$name = $_POST['name'];

 		$post_image = $_FILES['fileToUpload']['name'];
 		if ($post_image != "") {
 			$target_dir = "../documents/";
 			$target_file = $target_dir .uniqid(). basename($post_image);
 			$file = basename($post_image);
 			$uploadOk = 1;
 			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

			// Check if file already exists
 			if (file_exists($target_file)) {
 				echo "Sorry, file already exists.";
 				$uploadOk = 0;
 			}
			// Check file size
 			if ($_FILES["fileToUpload"]["size"] > 5000000) {
 				echo "Sorry, your file is too large.";
 				$uploadOk = 0;
 			}
			// Allow certain file formats
 			if($imageFileType != "doc" && $imageFileType != "pdf" && $imageFileType != "xlsx"
 				&& $imageFileType != "docx" ) {
 				echo "Sorry, only doc, pdf, xls & GIF files are allowed.";
 			$uploadOk = 0;
 		}
			// Check if $uploadOk is set to 0 by an error
 		if ($uploadOk == 0) {
 			echo "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
 		} 
 		else {
 			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
 				echo "The file ". basename($post_image). " has been uploaded.";
 				$query = mysqli_query($con, "INSERT INTO download(name, file_path, file) VALUES( '$name','$target_file', '$file')");
			 		if ($query) {
			 			$l = url_for('admin/index.php?url=download');
			 			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=download';window.location.href=e;</script>";
						header("Location: $l ");
			 		}
 			} else {
 				echo "Sorry, there was an error uploading your file.";
 			}
 		}	
 	}
 }
}

function submitResultFile($con){
    if (isset($_POST['submit'])) {
 		$name = $_POST['name'];

 		$post_image = $_FILES['fileToUpload']['name'];
 		if ($post_image != "") {
 			$target_dir = "../documents/";
 			$target_file = $target_dir .uniqid(). basename($post_image);
 			$file = basename($post_image);
 			$uploadOk = 1;
 			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

			// Check if file already exists
 			if (file_exists($target_file)) {
 				echo "Sorry, file already exists.";
 				$uploadOk = 0;
 			}
			// Check file size
 			if ($_FILES["fileToUpload"]["size"] > 5000000) {
 				echo "Sorry, your file is too large.";
 				$uploadOk = 0;
 			}
			// Allow certain file formats
 			if($imageFileType != "pdf") {
 				echo "Sorry, only pdf files are allowed.";
 			$uploadOk = 0;
 		}
			// Check if $uploadOk is set to 0 by an error
 		if ($uploadOk == 0) {
 			echo "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
 		} 
 		else {
 			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
 				echo "The file ". basename($post_image). " has been uploaded.";
 				$query = mysqli_query($con,"SELECT MAX(`id`) AS max FROM result");
 				$result=mysqli_fetch_assoc($query);
 				$oldId=$result['max'];
 				$query = mysqli_query($con,"UPDATE `result` SET `status`=0 WHERE `id`='$id'");
 				$query = mysqli_query($con, "INSERT INTO result(name, file_path, file,status) VALUES( '$name','$target_file', '$file',1)");
			 		if ($query) {
			 			$l = url_for('admin/index.php?url=result');
			 			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=result';window.location.href=e;</script>";
						header("Location: $l ");
			 		}
 			} else {
 				echo "Sorry, there was an error uploading your file.";
 			}
 		}	
 	}
 }
}

function deleteFile($con, $file_id){
	if (isset($_POST["del_yes"])) {
	    if(($_GET["fn"])=="download" ){
	        $query = mysqli_query($con, "DELETE FROM download WHERE id='".db_escape($con, $file_id)."' LIMIT 1" );
    		if ($query) {
    			$l = url_for('admin/index.php?url=download');
    			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=download';window.location.href=e;</script>";
    // 			header("Location: $l ");
    		}
	    }
	    if(($_GET["fn"])=="result"){
	        $query = mysqli_query($con, "DELETE FROM result WHERE id='".db_escape($con, $file_id)."' LIMIT 1" );
    		if ($query) {
    			$l = url_for('admin/index.php?url=result');
    			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=result';window.location.href=e;</script>";
    			header("Location: $l ");
    		}
	    }
	    if(($_GET["fn"])=="jfss"){
	        if(($_GET["option"])=="delete"){
	            $query = mysqli_query($con, "DELETE FROM jfss WHERE id='".db_escape($con, $file_id)."' LIMIT 1" );
    		    if ($query) {
    			    $l = url_for('admin/index.php?url=jfss');
    			    echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=jfss';window.location.href=e;</script>";
    			    header("Location: $l ");
    		    }
	        }
	        if(($_GET["option"])=="edit"){
	            $query = mysqli_query($con, " UPDATE jfss SET name='$body' WHERE id='".db_escape($con, $file_id)."' LIMIT 1" );
    		    if ($query) {
    			    $l = url_for('admin/index.php?url=jfss');
    			    echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=jfss';window.location.href=e;</script>";
    			    header("Location: $l ");
    		    }
	        }
	        
	    }
		
	}else{
		if (isset($_POST["del_no"])) {
		    if(($_GET["fn"])=="download" ){
		        echo "<script>
    	                var val=window.location.href; 
    	                val=val.split('/');
    	                val.pop();
    	                e=val.join('/');
    	                e=e+'/index.php?url=download';
    	                window.location.href=e;
		              </script>";
    	    }
    	    if(($_GET["fn"])=="result"){
    	        echo "<script>
    	                var val=window.location.href; 
    	                val=val.split('/');
    	                val.pop();
    	                e=val.join('/');
    	                e=e+'/index.php?url=result';
    	                window.location.href=e;
    	               </script>";
    	    }
    	    if(($_GET["fn"])=="jfss"){
    	        echo "<script>
    	                var val=window.location.href; 
    	                val=val.split('/');
    	                val.pop();
    	                e=val.join('/');
    	                e=e+'/index.php?url=jfss';
    	                window.location.href=e;
    	            </script>";
    	    }
			/*$l = url_for('admin/index.php?url=download');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=download';window.location.href=e;</script>";
			header("Location: $l");*/
		}
	}
}



function career($con){
	if (isset($_POST['submit'])) {
		$body = db_escape($con, $_POST['post-content']);

		$query=mysqli_query($con, "UPDATE career SET content='$body' WHERE id=1 ");
		if ($query) {
			$l = url_for('admin/index.php?url=career');
			echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');e=e+'/index.php?url=career';window.location.href=e;</script>";
			header("Location: $l ");
		}else{
			echo mysqli_error($con);
		}
	} 
}

?>