<!-- initialize -->
<?php require_once('../../private/initialize.php'); ?>
<!-- initialize end -->

<?php 
    ob_start();
   session_start();
	if (isset($_SESSION["valid"])) {
		header("Location: index.php");
	} 
	global $error_array;
	global $con;
	if (isset($_POST['adminLogin'])) {

		$email=filter_var(($_POST['email']), FILTER_SANITIZE_EMAIL); //sanitize email
		$email = db_escape($con, $email);
		$_SESSION['email']=$email;

 		$password = strip_tags($_POST['password']);//strips html tags
 		// $password =md5($password);
 		$password = db_escape($con, $password);

 		$query = mysqli_query($con, "SELECT * FROM cms_admin WHERE email='$email' AND password = '$password'");
 		$login_num_rows=mysqli_num_rows($query);

 		if ($login_num_rows == 1) {
 			$row = mysqli_fetch_assoc($query);
 			$username = $row['username'];
            $_SESSION['valid']= true;
 			$_SESSION['username'] = $username;
 		    echo "<script>var val=window.location.href; val=val.split('/');val.pop();e=val.join('/');window.location.href=e;</script>";
 			exit();
 			
 		}else{
 			array_push($error_array, "Your Email or Password is incorrect !<br>");
 		}
	}
	//admin login
	$title ="Admin Login"; 
?>


<!-- header -->
<?php include(SHARED_PATH.'/header.php'); ?>
<!-- header end -->		

 	<section class="admin">
 		<div class="container">
 			<div class="row">
 				<div class="col-md-4 offset-md-4">
 					<div class="admin-login text-center p-5">
 						<h3>Admin Login</h3>
 						<form action="" method="POST" autocomplete="off">
 							<div>
 								<input type="email" name="email" placeholder="email">
 							</div>
 							<div>
 								<input type="password" name="password">
 							</div>
 							<input type="submit" name="adminLogin" value="Login"><br>
							
							<span style="background: #eaeaea; font-size: 80%; border-radius: 3px; padding: 3px">
								<?php if (in_array("Your Email or Password is incorrect !<br>", $error_array)) {
 								echo "<span style='padding: 5px;'>Your Email or Password is incorrect !<br></span>";
 							} ?>
							</span>
 							
 						</form>
 					</div>
 				</div>
 			</div>
 		</div>
 	</section>


