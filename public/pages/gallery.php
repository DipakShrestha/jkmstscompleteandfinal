<style>
    #about-content{
        text-align: left;
        margin-top: 5%;
    }
    #about-content p{
        font-size: 17px;
        line-height: 1.5;
    }
    #about-content span{
        font-size: 150%;
        font-weight: 600;
        padding: 5px;
        border-bottom: 2px solid #004b8e;
    }
    #back{
        display: flex;
        width: 100%;
        flex-direction: row;
        /*position: absolute;*/
        /*   bottom:-2.5%;*/
        justify-content: space-between;
        background-color: transparen;
        border-width: 1px;
        border-style: solid;
        border-color: darkblue;
        overflow-x: scroll;
    }

    .image-style{
        max-width: 100px;
        min-width: 100px;
        max-height:100px;
        border-style: solid;
        border-color:transparent;
        opacity: 1;
        FILTER: BRIGHTNESS(.6);
        /*padding-left: 5px;*/
        /*padding-right: 5px;*/
    }
    .image-style:hover{
        /*filter: blur(2px);*/
        /*-webkit-filter: grayscale(100%); */
        /*   filter: grayscale(100%);*/
        /*FILTER: CONTRAST(3);*/
        FILTER: BRIGHTNESS(1);
    }

    /* width */
    ::-webkit-scrollbar {
        cursor: pointer;
        width: 12px;
        height: 8px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px grey;
        border-radius: 5px;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #004b8e;
        /*width:8px;*/
        border-radius: 10px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        /*background: #b30000; */
        background: blue;
    }

    .prev,
    .next {
        cursor: pointer;
        position: absolute;
        top: 40%;
        width: auto;
        padding: 16px;
        margin-top: -50px;
        color: white;
        font-weight: bold;
        font-size: 20px;
        border-radius: 0 3px 3px 0;
        user-select: none;
        -webkit-user-select: none;
    }

    /* Position the "next button" to the right */
    .next {
        right: 0;
        border-radius: 3px 0 0 3px;
    }

    /* On hover, add a black background color with a little bit see-through */
    .prev:hover,
    .next:hover {
        /*background-color: rgba(0, 0, 0, 0.8);*/
        background:#004b8e;
    }

    .image-view{
        background: #fff;
        border-style: solid;
        height:400px;
        border: #f00;
        width: cover;
    }

    .image-container{
        min-width: 100px;
        max-width:auto;
        display: flex;
        flex-direction: row;
        overflow:hidden;
        /*overflow-x: scroll;*/
        height: 400px;
    }
    #view-big-image{
        height:inherit;
        object-fit:cover;
        min-width:65vw;
    }

    @media (max-width: 350px){
        #view-big-image{
            height:inherit;
            object-fit:cover;
            min-width:40vw;
        }
    }

    @media (max-width: 615px){
        #view-big-image{
            height:inherit;
            object-fit:cover;
            min-width:85vw;
        }
    }

    @media (max-width: 750px){
        #view-big-image{
            height:inherit;
            object-fit:cover;
            min-width:85vw;
        }
    }

    @media (max-width: 1085px){
        #view-big-image{
            height:inherit;
            object-fit:cover;
            min-width:85vw;
        }
    }

</style>
<div id="about-content" style="margin-left: auto; margin-right:20px;">
    <ul style="width: 100%; position:relative ">
        <!-- <li class='gallert-li'>
             <a href='$path' data-toggle='lightbox' data-gallery='gallery'>
                 <img class='gallery-img image-style' src='$path' alt=''>
             </a>
         </li>";-->
        <div class="image-view">
            <div class="image-container" >
                <?php
                $id_image = 0;
                $paths = array();
                $count = 0;
                $query = mysqli_query($con, "SELECT * FROM gallery ORDER BY id DESC");
                while ($row = mysqli_fetch_assoc($query)) {
                    $id = $row['id'];

                    $path = $row['file_path'];
                    $paths[$count] = $path;
                    $count +=1;
                    $res ="
             		    <img id='view-big-image'  src='$path' alt=''>";
                    $id_image +=1;
                    echo $res;
                    // echo $path;
                }
                // echo $code;
                // print_r($paths);
                echo json_encode($paths,JSON_FORCE_OBJECT);
                $res = "<a class='prev' style='color:#fff;' onclick='plusSlides(-1)'>❮</a>
                    <a class='next' style='color:#fff;' onclick='plusSlides(1)'>❯</a>";
                echo $res;
                ?>
            </div>
        </div>
        <!--<a class="prev" style='color:#fff;' onclick="plusSlides(-1)">❮</a>
        <a class="next" style='color:#fff;' onclick="plusSlides(1)">❯</a>-->
        <div id="back">
            <?php
            $query = mysqli_query($con, "SELECT * FROM gallery ORDER BY id DESC");
            $val = 0;
            while ($row = mysqli_fetch_assoc($query)) {
                $id = $row['id'];
                $val += 1;
                $path = $row['file_path'];
                $res ="<li >
                        <a onclick='currentSlide($val,`$path`)'  >
                            <img class='gallery-img image-style' id='' name='small_image' src='$path' alt=''>
                        </a>
                    </li>";

                echo $res;
            }
            ?>
        </div>
        <script type="text/javascript">
            var slideIndex = 0;
            var viewImage = document.getElementById("view-big-image");
            var paths = <?php echo json_encode($paths) ?>;
            var len = paths.length;
            // var smallImage = document.getElementsByName("small_image");
            function plusSlides(n){
                if(n==-1){
                //   alert(JSON.stringify(paths[len-1]) +" "+  paths[18]);
                   if(slideIndex==0){
                       viewImage.src = paths[len];
                       slideIndex=len;
                   }
                   if(slideIndex>0){
                       viewImage.src = paths[slideIndex-1];
                       slideIndex = slideIndex - 1;
                   }
                }
                if(n==1){
                    // alert(JSON.stringify(paths[0]) + len);
                    if(slideIndex==len-1){
                        // alert(slideIndex);
                       viewImage.src = paths[0];
                        slideIndex=0;
                   }
                   else if(slideIndex>=0 && slideIndex<len-1){
                       viewImage.src = paths[slideIndex+1];
                       slideIndex = slideIndex + 1;
                   }
                }
            }
            function currentSlide(n,ath){
                // alert('value: '+ ath);
                // alert('value'+ n);
                // alert(ath);
                if(n==0){
                    slideIndex == n;
                }else{
                    slideIndex = n-1;
                }
                
                viewImage.src = ath;
                // smallImage.id = n;
                document.getElementById(n).style.borderColor = "#000" ;
            }

        </script>

        <!--href='$path' data-toggle='lightbox' data-gallery='gallery'-->
    </ul>
</div>
<div class="clear-fix"></div>
<div class="mb-5"></div>




