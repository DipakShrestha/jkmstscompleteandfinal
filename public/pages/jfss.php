<style>
    #about-content{
    	text-align: left;
    	margin-top: 5%;
    }
    #about-content p{
    	font-size: 17px;
    	line-height: 1.5;
    }
    #about-content span{
    	font-size: 150%;
    	font-weight: 600;
    	padding: 5px;
    	border-bottom: 2px solid #004b8e;
    
    }
    #title-view{
        width: 15vw;
        background: #2f506c;
        color: #fff;
        border-radius: 10px 30px 30px 10px;
        text-align: center;
    }
    #only-border{
        width: 100%;
        border-style: solid;
        border-color: transparent transparent #004b8e transparent;
        border-width: 1px;
        padding-bottom: 10px;
    }
</style>
<div id="about-content" style="display:flex; flex-direction:column;  text-align: center;">
	<span>Janakalyan Former Student Society Committee</span>
	<?php 
	$query = mysqli_query($con, "SELECT * FROM jfss " );
	while ($row = mysqli_fetch_assoc($query)) {
		$content = $row['name'];
        $date = $row['date'];
        $ret = "<div id='title-view' style='margin-top: 10px;'>".$date."</div>
                <div id='only-border'></div>";
        echo $ret;
        $res ="<div style='margin-top: 20px;'>".$content."</div>";
		echo $res;
	}
	?>
</div>

