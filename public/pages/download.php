<style>
#about-content{
	text-align: left;
	margin: 5% 0;
}
#about-content p{
	font-size: 17px;
	line-height: 1.5;
}
#about-content span{
	font-size: 150%;
	font-weight: 600;
	padding: 5px;
	border-bottom: 2px solid #004b8e;

}
</style>
<div id="about-content">
	
	<div class="card">
		<table class="table table-striped">
			<tr>
				<th>S.no</th>
				
				<th>File Name</th>
				<th>Download</th>
			</tr>
			<?php 
    			$query = mysqli_query($con, "SELECT * FROM download");
    			$counter =1;
    			while ($row = mysqli_fetch_assoc($query)) {
    				$id= $row['id'];
    				$name = $row['name'];
    				$filepath = $row['file_path'];
    				$res ="<tr class='view'>
    				<td>$counter</td>
    				<td>".ucwords($name)."</a></td>
    				<td><a class='btn btn-sm btn-primary' download href='$filepath'>Download</a></td>
    				</tr>";
    				echo $res;			
    				++$counter;
    			} 
    	    ?>
			
		</table>
	</div>
</div>

