<style>
    #title-view{
        width: 10vw;
        background: #2f506c;
        color: #fff;
        border-radius: 10px 30px 30px 10px;
        text-align: center;
    }
    #only-border{
        width: 100%;
        border-style: solid;
        border-color: transparent transparent #004b8e transparent;
        border-width: 1px;
        padding-bottom: 10px;
    }
</style>
<?php 
   
    if(isset($_POST['search'])){
        $searchText=$_POST['search'];
        if(strlen($searchText)>2){
            if($con->connect_error){
            echo 'Connection Faild: '.$con->connect_error;
            }else{
                $sql = "select *, 'events' from events where title like '%$searchText%' or date like '%$searchText%' or body like '%$searchText%';";
                // $sql .= "select *,'message' from message where name like '%$searchText%' or message like '%$searchText%'; ";
                // $sql .= "select *, 'about_us' from about_us where details like '%$searchText%' or history like '%$searchText%' or profile like '%$searchText%' or management like '%$searchText%'; ";
                $sql .= "select *, 'career' from career where content like '%$searchText%'; ";
                // $sql .= "select *, 'categories' from categories where name like '%$searchText%'; ";
                $sql .= "select *, 'download' from download where name like '%$searchText%' or file like '%$searchText%'; ";
                $sql .= "select *, 'facilities' from facilities where name like '%$searchText%' or content like '%$searchText%';";
                $sql .= "select *, 'news' from news where title like '%$searchText%' or content like '%$searchText%' or date like '%$searchText%';";
                // $sql .= "select *, 'programs' from programs where title like '%$searchText%' or content like '%$searchText%'";
                $sql .= "select *, 'programs' from programs where title like '%$searchText%'";
                if(mysqli_multi_query($con,$sql))
                {
                    do
                    {                   
                        if ($result=mysqli_store_result($con)) {
                             $counter =1;
                            while ($row=mysqli_fetch_row($result))
                                {
                                // printf("%s\t,%s\t,%s\t,%s\t,%s\t \n",$row[0], $row[1], $row[2], $row[3], $row[4]);
                                // echo end($row);
                                    if(end($row)=='events'){
                                        
                                        
                                          $id = url($row[0]);
                                          $event_title = $row[2];
                                          $event_body = $row[3];
                                          $date = $row[1];
                                          $yrdata = strtotime($date);
                                            
                                          $res ="<div class='row event-list py-2'>
                                          <div class='col-lg-4'>
                                          <ul class='event-day text-center border'>
                                          <li class='bg-event py-2' id='nepMonth".$counter."'>".date('F', $yrdata)."</li>
                                          <li class='nepDay bg-light py-2' id='nepDay".$counter."'>".date('jS', $yrdata)."</li>
                                          </ul>
                                          
                                          </div>
                                          <div class='col-lg-8'>
                                          <b>$event_title</b>
                                          <p>$event_body</p>
                                          <hr>
                                          </div>
                                        </div><script type='text/javascript'>
                                            var date='".$date."';date=ad2bs(date.split('-').join('/'));
                                            document.getElementById('nepMonth".$counter."').innerHTML=date.ne.strMonth;
                                            document.getElementById('nepDay".$counter."').innerHTML=date.ne.day;
                                        </script>";
                                        $ret = "<div id='title-view' >Events:</div>
                                                <div id='only-border'></div>";
                                        echo $ret;
                                        echo $res;
                                        ++$counter;
                                    }
                                    // if(end($row)=='message'){
                                        
                                    // }
                                    // if(end($row)=='about_us'){
                                    	
                                    // }
                                    if(end($row)=='career'){
                                        $content = $row[1];
                                        $ret = "<div id='title-view' >Career:</div>
                                                <div id='only-border'></div>";
                                        echo $ret;
    		                            echo $content;
                                    }
                                    // if(end($row)=='categories'){
                                        
                                    // }   
                                    if(end($row)=='download'){
                                        $content = $row[3];
                                        $ret = "<div id='title-view' >Download:</div>
                                                <div id='only-border'></div>";
                                        echo $ret;
    		                            echo $content;
                                    }
                                    if(end($row)=='facilities'){
                                        $id = $row[0];
                                        $title = $row[1];
                                        $image = $row[2];
                                        $content = $row[3];
                                        $content = substr($content,0, 80);
                                    
                                        $res ="<div class='row news-list py-2'>
                                            <div class='col-lg-4'>
                                            <div class='news-img'>
                                            <img src='$image' class='img-fluid' alt=''>
                                            </div> 
                                            </div>
                                            <div class='col-lg-8'>
                                            <b>".ucwords($title)."</b>
                                            <p> $content ...</p>
                                            <a href='".url_for('/pages?url=facilities&id='.$id.'')."' class='btn btn-sm btn-outline-primary'>Read More</a>
                                            </div>
                                            </div>";
                                    
                                        $ret = "<div id='title-view' >Facilities:</div>
                                                <div id='only-border'></div>";
                                        echo $ret;
                                        echo $res;
                                    }
                                    if(end($row)=='news'){
                                        
                                        $image = $row[3];
                                        $title = $row[1];
                                        $content = $row[4];
                                        $content = substr($content,0, 80);
                                        $id = $row[0];
                                        $res ="<div class='row news-list py-2'>
                                            <div class='col-lg-4'>
                                            <div class='news-img'>
                                            <img src='$image' class='img-fluid' alt=''>
                                            </div> 
                                            </div>
                                            <div class='col-lg-8'>
                                            <b>".ucwords($title)."</b>
                                            <p> $content ...</p>
                                            <a href='".url_for('/pages?url=news&id='.$id.'')."' class='btn btn-sm btn-outline-primary'>Read More</a>
                                            </div>
                                            </div>";
                                        $ret = "<div id='title-view' >News:</div>
                                                <div id='only-border'></div>";
                                        echo $ret;
                                        echo $res;
                                       
                                    }
                                    if(end($row)=='programs'){
                                        $ret = "<div id='title-view' >Programs:</div>
                                                <div id='only-border'></div>";
                                        echo $ret;
                                        viewProgram($con,$row[0]);
                                        // echo 'i am programs';
                                        // printf("\n");
                                    }
                                    
                                
                                    // echo count($row);
                                }
                        
                            mysqli_free_result($result);
                        }
                    }
                    while (mysqli_next_result($con));
                }
            }
            
            switch (strtolower($searchText)) {
					case "about us":
					    $ret = "<div id='title-view' >About Us:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					    include(PUBLIC_PATH."/pages/aboutus.php");
					break;
					
					case "event":
					    function Events($con){
                            $query = mysqli_query($con, "SELECT * FROM events ORDER BY date");
                            $counter =1;
                            while ($row = mysqli_fetch_assoc($query)) {
                              $id = url($row['id']);
                              $event_title = $row['title'];
                              $event_body = $row['body'];
                              $date = $row['date'];
                              $yrdata = strtotime($date);
                
                              $res ="<div class='row event-list py-2'>
                              <div class='col-lg-4'>
                              <ul class='event-day text-center border'>
                              <li class='bg-event py-2' id='nepMonth".$counter."'>".date('F', $yrdata)."</li>
                              <li class='nepDay bg-light py-2' id='nepDay".$counter."'>".date('jS', $yrdata)."</li>
                              </ul>
                              
                              </div>
                              <div class='col-lg-8'>
                              <b>$event_title</b>
                              <p>$event_body</p>
                              <hr>
                              </div>
                              </div><script type='text/javascript'>
                                var date='".$date."';date=ad2bs(date.split('-').join('/'));
                                document.getElementById('nepMonth".$counter."').innerHTML=date.ne.strMonth;
                                document.getElementById('nepDay".$counter."').innerHTML=date.ne.day;
                              </script>";
                
                              echo $res;      
                              ++$counter;
                            }
                          }
                          $ret = "<div id='title-view' >Events:</div>
                                <div id='only-border'></div>";
                        echo $ret;
                          Events($con);
                    break;
                    
                    case "events":
					    function Events($con){
                            $query = mysqli_query($con, "SELECT * FROM events ORDER BY date");
                            $counter =1;
                            while ($row = mysqli_fetch_assoc($query)) {
                              $id = url($row['id']);
                              $event_title = $row['title'];
                              $event_body = $row['body'];
                              $date = $row['date'];
                              $yrdata = strtotime($date);
                
                              $res ="<div class='row event-list py-2'>
                              <div class='col-lg-4'>
                              <ul class='event-day text-center border'>
                              <li class='bg-event py-2' id='nepMonth".$counter."'>".date('F', $yrdata)."</li>
                              <li class='nepDay bg-light py-2' id='nepDay".$counter."'>".date('jS', $yrdata)."</li>
                              </ul>
                              
                              </div>
                              <div class='col-lg-8'>
                              <b>$event_title</b>
                              <p>$event_body</p>
                              <hr>
                              </div>
                              </div><script type='text/javascript'>
                                var date='".$date."';date=ad2bs(date.split('-').join('/'));
                                document.getElementById('nepMonth".$counter."').innerHTML=date.ne.strMonth;
                                document.getElementById('nepDay".$counter."').innerHTML=date.ne.day;
                              </script>";
                
                              echo $res;      
                              ++$counter;
                            }
                          }
                          $ret = "<div id='title-view' >Events:</div>
                                <div id='only-border'></div>";
                        echo $ret;
                          Events($con);
                    break;

					case "news":
					    $ret = "<div id='title-view' >News:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					    include(PUBLIC_PATH."/pages/news.php");
					break;

					case "facilities":
					    $ret = "<div id='title-view' >Facilities:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					    include(PUBLIC_PATH."/pages/facility.php");
					break;
					
					case "facility":
					    $ret = "<div id='title-view' >Facilities:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					    include(PUBLIC_PATH."/pages/facility.php");
					break;

					case "download":
					    $ret = "<div id='title-view' >Download:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					    include(PUBLIC_PATH."/pages/download.php");
					break;

					case "academic program":
					   // include(PUBLIC_PATH."/pages/program.php");
					   $ret = "<div style='width: 25vw;' id='title-view' >Acedemic Program:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					   viewProgram($con, 1);
					   viewProgram($con, 2);
					   viewProgram($con, 3);
					   viewProgram($con, 4);
					   viewProgram($con, 5);
					   viewProgram($con, 6);
					   viewProgram($con, 7);
					   viewProgram($con, 8);
					   viewProgram($con, 9);
					break;
					
					case "latest news":
					    $ret = "<div id='title-view' >Latest News:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					    LatestNewsPages($con);
					break;
					
					case "program":
					   $ret = "<div style='width: 25vw;' id='title-view' >Acedemic Program:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					   viewProgram($con, 1);
					   viewProgram($con, 2);
					   viewProgram($con, 3);
					   viewProgram($con, 4);
					   viewProgram($con, 5);
					   viewProgram($con, 6);
					   viewProgram($con, 7);
					   viewProgram($con, 8);
					   viewProgram($con, 9);
					break;
					
					case "programs":
					   $ret = "<div style='width: 25vw;' id='title-view' >Acedemic Program:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					   viewProgram($con, 1);
					   viewProgram($con, 2);
					   viewProgram($con, 3);
					   viewProgram($con, 4);
					   viewProgram($con, 5);
					   viewProgram($con, 6);
					   viewProgram($con, 7);
					   viewProgram($con, 8);
					   viewProgram($con, 9);
					break;

					case "gallery":
					    $ret = "<div id='title-view' >Gallery:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					    include(PUBLIC_PATH."/pages/gallery.php");
					break;
					
					case "jfss":
					    $ret = "<div id='title-view' >JFSS:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					    include(PUBLIC_PATH."/pages/jfss.php");
					break;

					case "career":
					    $ret = "<div id='title-view' >Career:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					    include(PUBLIC_PATH."/pages/career.php");
					break;
					
					case "result":
					    $ret = "<div id='title-view' >Result:</div>
                                <div id='only-border'></div>";
                        echo $ret;
					    include(PUBLIC_PATH."/pages/result.php");
					break;
					    
					default:
					    $ret = "<div id='title-view' >Search Says:</div>
                                <div id='only-border'></div>";
                        echo $ret;
    					$msg = "<div style='color:red; display:flex; flex-direction: row; text-align: center; padding-top: 20px; width: 100%;'>Cannot Find!!</div>";
                        echo $msg;
				}
        }
        else{
            $ret = "<div id='title-view' >Search Says:</div>
                    <div id='only-border'></div>";
            echo $ret;
            $msg = "<div style='color:red; display:flex; flex-direction: row; text-align: center; padding-top: 20px; width: 100%;'>Input must be greater than 2 letter!!</div>";
            echo $msg;
           
        }
        
       
    }else{
        header('Location:/public');
    }
?>
