<?php
    function renderResult($con){
        $query = mysqli_query($con, "SELECT * FROM result");
        while ($row = mysqli_fetch_assoc($query)) {
    		$id= $row['id'];
    		$name = $row['name'];
    		$file = $row['file'];
    		$path = $row['file_path'];
    		/*$res ="<div style='display: flex; flex-direction: column; text-align: center; align-item:center;'>
    		            <div style='background: #004b8e; padding-left: 10px; padding-right: 10px; font-size: 2em;
                                    border-radius: 10px 10px 0 0; margin-top:10px; color:white;'>$name</div>
                      <iframe src='$path'style='width:100%;height:700px;'></iframe>
                    </div>";*/
            $res  = '<div class="modal" id="myModal'.$id.'">
              <div class="modal-dialog">
                <div class="modal-content" style="color:#000">
            
                  <!-- Modal Header -->
                  <div class="modal-header">
                    <h4 class="modal-title" style="color: #004b8e;">'.$name.'</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
            
                  <!-- Modal body -->
                  <div class="modal-body" style="box-shadow: 0 0 0 5px rgba(200,200,200,0.1) inset, 0px 0 3px 0 rgba(0,0,0,0.35);border-radius:5px;margin:9px;">
                   
                    <div style="display: flex; flex-direction: column; text-align: center; align-item:center;">
                        <div style="background: #004b8e; padding-left: 10px; padding-right: 10px; font-size: 2em;
                                    border-radius: 10px 10px 0 0; margin-top:10px; color:white;">'.$name.'</div>
                        <object id="pdf-view" data="'.$path.'" type="application/pdf" width="100%" height="700" style="padding-bottom: 10px;">
                            <span style="color: white;">click here to view result</span> : <a style="color: red; border-style: solid; border-color: black;" href="'.$path.'">$name.pdf</a>
                        </object>
                    </div>
                  </div>
            
                  <!-- Modal footer -->
                  <div class="modal-footer">
                    <button type="button" class="btn btn-primary" style="color:#fff; background: #004b8e;" data-dismiss="modal">Close</button>
                  </div>
            
                </div>
              </div>
            </div>';
            /*$res ="<div style='display: flex; flex-direction: column; text-align: center; align-item:center;'>
                        <div style='background: #004b8e; padding-left: 10px; padding-right: 10px; font-size: 2em;
                                    border-radius: 10px 10px 0 0; margin-top:10px; color:white;'>$name</div>
                        <object id='pdf-view' data='$path' type='application/pdf' width='100%' height='700' style='padding-bottom: 10px;'>
                            <span style='color: white;'>click here to view result</span> : <a style='color: red; border-style: solid; border-color: black;' href='$path'>$name.pdf</a>
                        </object>
                    </div>";*/
    
    		echo $res;			
    		
    	}
    }
?>
<div class="card" style="margin-top:20px;">
	<table class="table table-striped">
		<tr>
			<th>S.no</th>
			
			<th>Name</th>
			<th>View</th>
			<th>Download</th>
		</tr>
		<?php 
			$query = mysqli_query($con, "SELECT * FROM result");
			$counter =1;
			
			while ($row = mysqli_fetch_assoc($query)) {
				$id= $row['id'];
				$name = $row['name'];
				$filepath = $row['file_path'];
				$res ="<tr class='view'>
				<td>$counter</td>
				<td>".ucwords($name)."</a></td>
				<td><a class='btn btn-sm btn-primary' style='color:#fff;' download data-toggle='modal' data-target='#myModal$id'>View</a></td>
				<td><a class='btn btn-sm btn-primary' download href='$filepath'>Download</a></td>
				</tr>";
				echo $res;			
				++$counter;
			} 
	    ?>
		
	</table>
	<?php
	    renderResult($con);
	 ?>
</div>
<style>
    /* width */
    ::-webkit-scrollbar {
        cursor: pointer;
        width: 12px;
        height: 8px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px grey;
        border-radius: 5px;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #004b8e;
        /*width:8px;*/
        border-radius: 10px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        /*background: #b30000; */
        background: blue;
    }
    
    #main-result{
        height: 800px; 
        overflow-y: scroll; 
        padding-right:20px; 
        padding-left: 20px; 
        background: gray; 
        border-radius: 10px 5px 5px 10px;
    }
    
    @media (max-width: 615px){
       #pdf-view{
           height: 200px;
       }
       #main-result{
           height: 300px;
       }
    }
</style>



 
 
