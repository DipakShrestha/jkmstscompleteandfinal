<!-- initialize -->
<?php require_once('../private/initialize.php'); ?>
<!-- initialize end -->
<?php $title ="Home"; ?>
<!-- header -->
<?php include(SHARED_PATH.'/header.php'); ?>
<!-- header end -->		

<style>
    .blink_me {
        animation: blinker 3s linear infinite;
        z-index: -1;
    }

    @keyframes blinker {
        50% {
            opacity: 0.3;
        }
    }
</style>

<!-- BANNER AREA -->
<div class="container">
    <div class="row" style="display:flex; flex-direction:row;">
     <marquee style="width:75%;"><p><?php marqueeNews($con)?></p></marquee>
     <?php 
        $query = mysqli_query($con,"SELECT MAX(`id`) AS max FROM result");
 		$result=mysqli_fetch_assoc($query);
 		$id=$result['max'];
        $query=mysqli_query($con,"SELECT date FROM result WHERE `id`='$id'");
        while($result=mysqli_fetch_assoc($query)){
            $curtime=time();
            $var=strtotime($result['date']);
            if($curtime-$var<=400000){ ?>
            <div  style="background: #004b8e;color:red;margin-left: 8px;padding-top:7px; border-radius: 5px 5px 0 0;"><span><a class="blink_me" style="text-decoration:none;color:yellow;"href="<?=url_for('/pages?url=result'); ?>">Result Published!!</a></span></div>
        <?php } 
     }?>
    </div>
<div class="container-fluid mb-5">
  <div class="row">
    <div class="col-md-9">
      <!-- carousel banner -->
      <div id="carousel-image" class="carousel slide" data-ride="carousel" style="margin-right: 20px; margin-top: 10px;">
        <ol class="carousel-indicators">
          <li data-target="#carousel-image" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-image" data-slide-to="1"></li>
          <li data-target="#carousel-image" data-slide-to="2"></li>
          <li data-target="#carousel-image" data-slide-to="3"></li>
          <li data-target="#carousel-image" data-slide-to="4"></li>
          <li data-target="#carousel-image" data-slide-to="5"></li>
          <li data-target="#carousel-image" data-slide-to="6"></li>
          <li data-target="#carousel-image" data-slide-to="7"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item cs-img1 active">
            <!-- <img class="d-block w-100"  src="img/banner/1.jpeg" alt="First slide"> -->
          </div>
          <div class="carousel-item cs-img2">
            <!-- <img class="d-block w-100" src="img/banner/2.jpeg" alt="Second slide"> -->
          </div>
          <div class="carousel-item cs-img3">
            <!-- <img class="d-block w-100" src="img/banner/3.jpeg" alt="Third slide"> -->
          </div>

          <div class="carousel-item cs-img6">
            <!-- <img class="d-block w-100" src="img/banner/3.jpeg" alt="Third slide"> -->
          </div>
          <div class="carousel-item cs-img7">
            <!-- <img class="d-block w-100" src="img/banner/3.jpeg" alt="Third slide"> -->
          </div>
          <div class="carousel-item cs-img8">
            <!-- <img class="d-block w-100" src="img/banner/3.jpeg" alt="Third slide"> -->
          </div>
          <div class="carousel-item cs-img9">
            <!-- <img class="d-block w-100" src="img/banner/3.jpeg" alt="Third slide"> -->
          </div>
          <div class="carousel-item cs-img10">
            <!-- <img class="d-block w-100" src="img/banner/3.jpeg" alt="Third slide"> -->
          </div>
        </div>


        <!-- next prev button -->
          <!-- <a class="carousel-control-prev" href="#carousel-image" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>


          <a class="carousel-control-next" href="#carousel-image" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a> -->
        </div>
      </div>
      <!-- carousel banner end -->


      <!-- banner-news -->
      <div class="col-md-3 ">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">News and Notices</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Events</a>
          </li> -->
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane tab-mod fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <?php bannerNews($con); ?>
          </div>
        </div>
      </div>
      <!-- banner-news ends-->
    </div>
  </div>
  <!-- BANNER END -->
  <hr class="style-four">


  <!-- About us -->
  <section class="about-us my-5">
    <h3>विध्यालयको संक्षिप्त परिचय</h3>
    <div class="container">
      <div class="row ">

      </div>
      <div class="row">
        <div class="col-sm-12 col-md-9">

          <div class="about-content py-4">
            <?php selectAboutUslim($con); ?>
            <div><a href="<?php echo url_for('/pages?url=about us&column=details'); ?>" class="btn btn-sm btn-primary">Read More</a></div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="about-image"></div>
        </div>
      </div>
    </div>
  </section>
  <!-- About us -->



  <!-- Message -->
  <section class="message py-5">
    <div class="container">
      <div class="row">
        <div class="col-lg-6" styl="text-align:center;">
          <div class="message-title">MESSAGE FROM Principle</div>
          <?php 
          messagePrincipal($con);
          ?>
        </div>
 
        <div class="col-lg-6">
          <div class="message-title">MESSAGE FROM CHAIRMAIN</div>

          <?php messageChairman($con); ?>
          
        </div>
      </div>
      
    </div>
  </section>
  <?php 
    function showfullPrincipalmessage($con){
        $query = mysqli_query($con, "SELECT * FROM message");
    	while ($row = mysqli_fetch_assoc($query)) {
    		$name = hsc($row['name']);
    		$id = $row['id'];
    		
    		$message = $row['message'];
    		$photo = $row['file_path'];
    		$photo = str_replace('../', '', $photo);
    		
            $ret = '<div class="modal" id="myModal'.$id.'">
                      <div class="modal-dialog">
                        <div class="modal-content" style="color:#000">
                    
                          <!-- Modal Header -->
                          <div class="modal-header">
                            <h4 class="modal-title" style="color: #004b8e;">'.ucwords($name).'</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                    
                          <!-- Modal body -->
                          <div class="modal-body" style="box-shadow: 0 0 0 5px rgba(200,200,200,0.1) inset, 0px 0 3px 0 rgba(0,0,0,0.35);border-radius:5px;margin:9px;">
                            <div class="message-img">
                        		<img src="'.$photo.'" alt="">
                        	</div>
                    		<p>'.$message.'</p>
                          </div>
                    
                          <!-- Modal footer -->
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" style="color:#fff; background: #004b8e;" data-dismiss="modal">Close</button>
                          </div>
                    
                        </div>
                      </div>
                    </div>';
    		echo $ret;
    	}
    }
    showfullPrincipalmessage($con);
    ?>
  

  <!-- Message End-->



  <!-- NEWS SECTION -->
  <section class="news">
    <div class="container">
      <div class="row">

        <!-- NEWS SECTION start -->
        <div class="col-lg-6 pb-2 border-right newsLeft">
          <div class="message-title"><i class="ion-ios-paper"></i>&nbsp;&nbsp;&nbsp;Latest News & Article</div>
          <div class="container" >

            <!-- Start here -->

            <?php 
            viewLatestNews($con);
            ?>
            <!-- end here -->

          </div>
        </div>
        <!-- NEWS SECTION start -->

        <hr class="style-four">

        <!-- Event SECTION start -->
        <div class="col-lg-6" >
          <div class="message-title"><i class="ion-android-calendar"></i>&nbsp;&nbsp;&nbsp;Upcomming Events</div>

          <?php 
          function Events($con){
            $query = mysqli_query($con, "SELECT * FROM events ORDER BY date");
            $counter =1;
            while ($row = mysqli_fetch_assoc($query)) {
              $id = url($row['id']);
              $event_title = $row['title'];
              $event_body = $row['body'];
              $date = $row['date'];
              $yrdata = strtotime($date);

              $res ="<div class='row event-list py-2' style=''>
                          <div class='col-lg-4' style='margin-top: 10px'>
                              <ul class='event-day text-center border'>
                                  <li class='bg-event py-2' id='nepMonth".$counter."'>".date('F', $yrdata)."</li>
                                  <li class='nepDay bg-light py-2' id='nepDay".$counter."'>".date('jS', $yrdata)."</li>
                              </ul>
                          </div>
                          <div class='col-lg-8' style='margin-top: 10px'>
                              <b>$event_title</b>
                              <p>$event_body</p>
                              <hr>
                          </div>
                    </div>
                  <script type='text/javascript'>
                    var date='".$date."';date=ad2bs(date.split('-').join('/'));
                    document.getElementById('nepMonth".$counter."').innerHTML=date.ne.strMonth;
                    document.getElementById('nepDay".$counter."').innerHTML=date.ne.day;
                  </script>";

              echo $res;      
              ++$counter;
            }
          }
            
          
          ?>
          <div style="height:80vh; width:inherit; overflow: hidden;overflow-y:scroll; ">
                <?php 
                    Events($con);
                ?>
          </div>
        </div>
        <!-- NEWS SECTION start -->



      </div>
    </div>
  </section>
  <!-- NEWS SECTION End-->
    <hr class="style-four">
  <div class="section py-4">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="autoplay-title">We Provide Innovative Education</div>
        </div>  
      </div>

      <div class="row" style=" padding:0;">
        <div class="col-lg-12" style="margin:0; padding:0">
          <!--<div class="slider mx-3">-->
          <div class="slider" style="margin: 0; padding: 0;">
            <div class="autoplay-image" style="margin-left: 5px;">
              <img src="img/slider/1.jpg" class="img-fluid" alt="abc">
            </div>
            <div class="autoplay-image" style="margin-left: 5px;">
              <img src="img/slider/2.jpg" class="img-fluid" alt="abc">
            </div>
            <div class="autoplay-image" style="margin-left: 5px;">
              <img src="img/slider/3.jpg" class="img-fluid" alt="abc">
            </div>
            <div class="autoplay-image" style="margin-left: 5px;">
              <img src="img/slider/4.jpg" class="img-fluid" alt="abc">
            </div>
            <div class="autoplay-image" style="margin-left: 5px;">
              <img src="img/slider/5.jpg" class="img-fluid" alt="abc">
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>


    <!--<hr class="style-four" style="margin-left: 1vw; margin-right: 1vw;">-->
    <hr class="" style="margin-left: auto; margin-right: auto; height: 5px; background-color:#c0c0c0; margin-top: 0; margin-bottom: 0;">
  <!-- testimonial -->
    <section class="testimonial" style="margin-left: auto; margin-right: auto;align-item:center; justify-content: center; height:fit-content;" >
        <div class="testimonial-overlay py-5">
            <h3>Testimonials</h3>
            <div class="container-fluid">
                <div class="" style="display:block; flex-direction: row; width:100%; ">
                   
                    <div style="width:inherit;display:flex; flex-direction:row;  overflow:hidden; overflow-x:scroll;">
                        <?php 
                            fetchTestimonial($con);
                        // echo $extra_name;
                        ?>
                    </div>
                    
                </div>
            </div>
        </div>
        <?php 
            fetchTestimonialContent($con);
            // echo $extra_name;
        ?>
        <style>
            /* width */
            ::-webkit-scrollbar {
                cursor: pointer;
                width: 8px;
                height: 8px;
            }
        
            /* Track */
            ::-webkit-scrollbar-track {
                box-shadow: inset 0 0 5px grey;
                border-radius: 5px;
            }
        
            /* Handle */
            ::-webkit-scrollbar-thumb {
                background: #004b8e;
                /*width:8px;*/
                border-radius: 10px;
            }
        
            /* Handle on hover */
            ::-webkit-scrollbar-thumb:hover {
                /*background: #b30000; */
                background: blue;
            }
        </style>
        <script>
            function openModal(name){
                alert(name);
            }
        </script>
    </section>




<!-- footer -->
<div style="margin-left: auto; margin-right: auto;">
   <?php include(SHARED_PATH.'/footer.php'); ?>
</div>

